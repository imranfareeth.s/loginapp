<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class validate_lib
{
	function validate_json_input($jsonInput, $errDTO)
	{
		if(count($jsonInput) < 1 || !isset($jsonInput['user_id']))
		{
			$errDTO->setErrorCode('ER_01');
			$errDTO->setErrorMessage('Bad request! Invalid request!');
			return false;
		}
		return true;
	}
	
	function validate_json_login_input($jsonInput, $errDTO)
	{
		if(count($jsonInput) < 1 || !isset($jsonInput['user_name']))
		{
			$errDTO->setErrorCode('ER_001');
			$errDTO->setErrorMessage('Bad request! Invalid request!');
			return false;
		}
		return true;
	}
	
	
}