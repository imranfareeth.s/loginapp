<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/***
 * Class ErrorResponseDto DTO
 * Description: 
 * Created on: 27-02-2016
 *
 */
class ErrorResponseDto implements \JsonSerializable {
	
	private $errorCode;
	
	private $errorMessage;
	
	//private $errorDetails = array();

    public function getErrorCode() {
        return $this->errorCode;
    }
    public function setErrorCode($errorCode) {
        $this->errorCode = $errorCode;
    }
	
	public function getErrorMessage() {
        return $this->errorMessage;
    }
    public function setErrorMessage($errorMessage) {
        $this->errorMessage = $errorMessage;
    }
	

	
	public function JsonSerialize()
    {
        $vars = get_object_vars($this);
        return $vars;
    }
	
	
}