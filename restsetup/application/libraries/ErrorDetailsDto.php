<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ErrorDetailsDto implements \JsonSerializable{

	private $msg;
	
	public function setMsg($msg)
	{
		$this->msg = $msg;
	}
	
	public function getMsg()
	{
		return $this->msg;
	}
	
	public function JsonSerialize()
    {
        $vars = get_object_vars($this);
        return $vars;
    }

}