<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'libraries/REST_Controller.php');
require(APPPATH.'libraries/ErrorResponseDto.php');
require(APPPATH.'libraries/ErrorDetailsDto.php');
require(APPPATH.'libraries/validate_lib.php');

 
class Login extends REST_Controller {
	
	const apiSuccess = "success";
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('api_01/authentication/Login_model');
		$this->load->database();

    }	

	public function Validate_post()
	{
		try
		{
			$errDTO = new ErrorResponseDto();
			$validateLib = new validate_lib();
			$userRequest = json_decode(file_get_contents('php://input'), TRUE);
			$vResp = $validateLib->validate_json_login_input($userRequest, $errDTO);
			
			if(!$vResp)
			{
				$this->response($errDTO, 400);
				die();
			}

			$user_name = $this->security->xss_clean($userRequest['user_name']);
			$userpassword = $this->security->xss_clean($userRequest['user_password']);

			$userDetails = $this->Login_model->login_validate($user_name,$userpassword);

			
			if(isset($userDetails) && $userDetails['api_status'] == self::apiSuccess)
			{
				$this->response($userDetails, 200);
				die();
			}	
			$errDTO = new ErrorResponseDto();
			$errDTO->setErrorCode('ER_01 ');
			$errDTO->setErrorMessage('Invalid user name or password!');
			throw new Exception($this->response($errDTO, 400));		
			
		}
		catch (Exception $e){			
		    echo $e->getMessage();				   			
		}		
	
	}
	
	public function Register_post()
	{
		try
		{
			$errDTO = new ErrorResponseDto();
			$validateLib = new validate_lib();
			$userRequest = json_decode(file_get_contents('php://input'), TRUE);
			$vResp = $validateLib->validate_json_login_input($userRequest, $errDTO);
			
			if(!$vResp)
			{
				$this->response($errDTO, 400);
				die();
			}

			$fullname = $this->security->xss_clean($userRequest['fullname']);
			$user_name = $this->security->xss_clean($userRequest['user_name']);
			$userpassword = $this->security->xss_clean($userRequest['user_password']);
			$key = gmdate('D, d M Y H:i:s T', time()).$fullname;
			$key = sha1($key);
			$userDetails = $this->Login_model->register($fullname,$user_name,$userpassword);
			
			if(isset($userDetails) && $userDetails['api_status'] == self::apiSuccess)
			{
				$this->response($userDetails, 200);
				die();
			}
			
			$errDTO = new ErrorResponseDto();
			$errDTO->setErrorCode('ER_02 ');
			$errDTO->setErrorMessage('Already exist');
			throw new Exception($this->response($errDTO, 400));		
			
		}
		catch (Exception $e){			
		    echo $e->getMessage();				   			
		}		
	
	}
}
