<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {

	public function Login_validate($user_name,$userpassword)
	{
		$this->db->select('id,fullname');
		$this->db->where('user_name',$user_name);
		$this->db->where('password',md5($userpassword));
		$query = $this->db->get('userAuthen ua');
		$respArr['api_status']="fail";
		if($query->num_rows() > 0)
		{	
			$result = $query->row_array();
			$respArr['api_status']="success";
			$respArr['id']=$result['id'];
			$respArr['fullname']=$result['fullname'];
			$key = gmdate('D, d M Y H:i:s T', time()).$user_name;
			$key = sha1($key);
			$respArr['session_token']=$key;
			$this->db->where('id',$result['id']);
			$this->db->update('userSession',array("session"=>$key));
			
			
		}
		return $respArr;
	}	

	public function Register($fullname,$user_name,$userpassword)
	{
		$this->db->select('id');
		$this->db->where('user_name',$user_name);
		$query = $this->db->get('userAuthen');
		$respArr['api_status']="fail";
		
		if($query->num_rows() == 0)
		{
			$resArray = array(
							"fullname"=>$fullname,
							"user_name"=>$user_name,
							"password"=>md5($userpassword),
							"status"=>"Y"
						);
			$this->db->insert('userAuthen',$resArray);
			$id = $this->db->insert_id();
			$array = array(
						"id"=>$id,
						"user_name"=>$user_name
						);
			$this->db->insert('userSession',$array);
			$respArr['api_status']="success";
		}
		
		return $respArr;
		
	}
}
	
	