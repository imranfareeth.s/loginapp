import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ValidateService } from './../validate.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Http, Headers, RequestOptions } from '@angular/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  constructor(private route:Router, private valid:ValidateService, private http:HttpClient ) { }
  private status;
  private errMsg;
  private name;
  ngOnInit() {
    console.log(this.valid.islogged)
  }

  validate(e)
  {
    var username = e.target.elements[0].value;
    var password = e.target.elements[1].value;
    if(username !=='' && password !==''){
      const req = this.http.post('http://localhost/restsetup/api_01/authentication/login/validate', JSON.stringify({
        user_name: username,
        user_password: password
      }))
        .subscribe(
          res => {
            this.status = res;
            console.log(res);
            if(this.status.api_status == 'success')
            {
              this.valid.islogged = true;
              this.valid.fullname = this.status.fullname;
              this.route.navigate(['welcome']);
            }
          },
          err => {
              this.errMsg = "Invalid Username and Password";
          }
        );
    } else {
      this.errMsg = "Enter the Username and Password";
    }




  }

}
