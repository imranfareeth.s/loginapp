import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
 private errMsg
 private status;
private form;
  constructor(private route:Router, private http:HttpClient) {
      this.form = new FormGroup({
        'fullname' : new FormControl('',Validators.required),
        'username' : new FormControl('',Validators.required),
        'password' : new FormControl('',Validators.required)
      });
  }

  ngOnInit() {
  }

  register(e)
  {
    var fullname = e.target.elements[0].value;
    var username = e.target.elements[1].value;
    var password = e.target.elements[2].value;

    const req = this.http.post('http://localhost/restsetup/api_01/authentication/login/register', {
      fullname: fullname,
      user_name: username,
      user_password: password
    })
      .subscribe(
        res => {
          this.status = res;
          if(this.status.api_status == 'success')
          {
            this.route.navigate(['']);
          }
        },
        err => {
            this.errMsg = "Already username exist";
        }
      );



  }

}
