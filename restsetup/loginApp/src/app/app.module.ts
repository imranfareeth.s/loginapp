import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './login/login.component';

import { RouterModule, Routes} from '@angular/router';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { WelcomeComponent } from './welcome/welcome.component';
import { ValidateService } from './validate.service';
import { RegisterComponent } from './register/register.component';
import { HttpModule} from '@angular/http';


const appRoutes: Routes = [
   {
      path: '',
      component: LoginComponent
   },
   {
      path: 'welcome',
      component: WelcomeComponent
   },
   {
     path: 'register',
     component: RegisterComponent
   }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    WelcomeComponent,
    RegisterComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpModule

  ],
  providers: [ValidateService],
  bootstrap: [AppComponent]
})
export class AppModule { }
