import { Component, OnInit } from '@angular/core';
import { ValidateService } from './../validate.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {
  private fullname;
  private isvalid;
  constructor(private valid:ValidateService, private router:Router) { }

  ngOnInit() {
     this.isvalid = this.valid.islogged;
     this.fullname =  this.valid.fullname;
     
  }

  logout()
  {
    this.valid.islogged = false;
    this.router.navigate(['']);
  }



}
